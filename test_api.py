import requests

username = 'khawarkhan'
token = '8fb50f162002d7826b37ba0b83dc36e689ec91ea'

url = f'https://www.pythonanywhere.com/api/v0/user/{username}/cpu/'
headers = {'Authorization': f'Token {token}'}

response = requests.get(url, headers=headers)

if response.status_code == 200:
    print('CPU quota info:')
    print(response.content)
else:
    print(f'Got unexpected status code {response.status_code}: {response.content}')
